from django.apps import AppConfig


class ConfigCenterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'config_center'
